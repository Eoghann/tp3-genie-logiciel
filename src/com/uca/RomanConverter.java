package com.uca;


import java.util.Collection;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class RomanConverter{
	
	// Table des symboles
	private static final Collection<RomanNumber> SYMBOLS = new ArrayList<>();
	static {
		SYMBOLS.add(new RomanNumber(1000, "M"));
		SYMBOLS.add(new RomanNumber(900, "CM"));
		SYMBOLS.add(new RomanNumber(500, "D"));
		SYMBOLS.add(new RomanNumber(400, "CD"));
		SYMBOLS.add(new RomanNumber(100, "C"));
		SYMBOLS.add(new RomanNumber(90, "XC"));
		SYMBOLS.add(new RomanNumber(50, "L"));
		SYMBOLS.add(new RomanNumber(40, "XL"));
		SYMBOLS.add(new RomanNumber(10, "X"));
		SYMBOLS.add(new RomanNumber(9, "IX"));
		SYMBOLS.add(new RomanNumber(5, "V"));
		SYMBOLS.add(new RomanNumber(4, "IV"));
		SYMBOLS.add(new RomanNumber(1, "I"));
	  }

	// Expression reguliere de validation
	private static final Pattern VALIDATION_RE = Pattern.compile("^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$");

	public static String getRomanFromNumber(int a) throws IllegalArgumentException{
		String res = "";

		if (a < 1 || a >= 4000){
			throw new IllegalArgumentException();
		}

		for (RomanNumber romn : SYMBOLS){
			while(a >= romn.getValue()){
				res += romn.getRoman();
				a -= romn.getValue();
			}
		}

		return res;
	}
	

	public static int getNumberFromRoman(String a) throws IllegalArgumentException{

		int res = 0;
		int index = 0;

		if(!(a.matches(String.valueOf(VALIDATION_RE)))){
			throw new IllegalArgumentException();
		}

		/*if (a != a.toUpperCase()){
			throw new IllegalArgumentException();
		}

		for (int i = 0; i < a.length(); i++) {

			if(a.length() == 2 && i == 0){
				if ("LVD".contains(String.valueOf(a.charAt(i)))) {
					if(a.charAt(i+1) == a.charAt(i)) {
						throw new IllegalArgumentException();
					}
				}
			}

			if (i > 0 && i < a.length()-1) {
				if ("LVD".contains(String.valueOf(a.charAt(i)))) {
					if(a.charAt(i-1) == a.charAt(i) || a.charAt(i+1) == a.charAt(i)) {
						throw new IllegalArgumentException();
					}
				} else if (i < a.length()-2) {
					if(a.charAt(i-1) == a.charAt(i) && a.charAt(i) == a.charAt(i+1) && a.charAt(i) == a.charAt(i+2)) {
						throw new IllegalArgumentException();
					}
				}
			}
		}*/

		for (RomanNumber romn : SYMBOLS){
			//try{
			while(index + romn.getRoman().length() <= a.length() && a.substring(index, index + romn.getRoman().length()).equals(romn.getRoman())){
				res += romn.getValue();
				index += romn.getRoman().length();
			}
//			}
//			catch(StringIndexOutOfBoundsException e){
				
//			}
		}
		return res;
	}
}