package com.uca;

public class Start{
	
	//Start class
	public static void main(String[] args){
		
		RomanNumber roman = new RomanNumber("IV");
		System.out.println(roman.getValue());
		System.out.println(roman.getRoman());
		System.out.println(roman);
		System.out.println("Double value : " + roman.doubleValue());
		System.out.println("Float value : " + roman.floatValue());
		System.out.println("Int value : " + roman.intValue());
		System.out.println("Long value : " + roman.longValue());
		System.out.println("int " + roman.compareTo(1));
		System.out.println("double " + roman.compareTo(1.0));
		System.out.println("float " + roman.compareTo(4.0f));
		System.out.println("long " + roman.compareTo(1L));
		System.out.println("roman " + roman.compareTo(new RomanNumber("VII")));
		//TODO
		//Aide pour démarrer : https://git.artheriom.fr/l3-informatique-2020-2021/site-l3/-/tree/master/Genie_Logiciel/HelperTP3
		//Aussi : https://www.youtube.com/watch?v=567_hWQJYak
	}
	
}