package com.uca;

import org.junit.jupiter.api.Test;
import java.util.concurrent.Callable;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Tests {
	
	@Test
	public void testConverter(){

		assertThat(RomanConverter.getRomanFromNumber(7), equalTo("VII"));
		assertThat(exceptionOf(() -> RomanConverter.getRomanFromNumber(-2)), instanceOf(IllegalArgumentException.class));


	}

    @Test
    public void TestAFaire(){
        
        assertThat(RomanConverter.getNumberFromRoman("VII"), equalTo(7));
        //assertThat(exceptionOf(() -> RomanConverter.getRomanFromNumber(4.5)), instanceOf(IllegalArgumentException.class));
        assertThat(exceptionOf(() -> RomanConverter.getRomanFromNumber(0)), instanceOf(IllegalArgumentException.class));
        assertThat(exceptionOf(() -> RomanConverter.getRomanFromNumber(4000)), instanceOf(IllegalArgumentException.class));
        assertThat(exceptionOf(() -> RomanConverter.getNumberFromRoman("LL")), instanceOf(IllegalArgumentException.class));
        assertThat(exceptionOf(() -> RomanConverter.getNumberFromRoman("IIII")), instanceOf(IllegalArgumentException.class));
        assertThat(exceptionOf(() -> RomanConverter.getNumberFromRoman("XCX")), instanceOf(IllegalArgumentException.class));

        assertThat(RomanConverter.getNumberFromRoman(RomanConverter.getRomanFromNumber(7)), equalTo(7));
        assertThat(RomanConverter.getRomanFromNumber(7), equalTo(RomanConverter.getRomanFromNumber(7).toUpperCase()));
        assertThat(exceptionOf(() -> RomanConverter.getNumberFromRoman("Vii")), instanceOf(IllegalArgumentException.class));

        RomanNumber roman = new RomanNumber("VII");

        //Tests méthodes classe Number
        assertThat(roman.intValue() , equalTo(7));
        assertThat(roman.doubleValue() , equalTo(7.0));
        assertThat(roman.floatValue() , equalTo(7.0f));
        assertThat(roman.longValue() , equalTo(7L));

        //Tests compareTo sur LONG
        assertThat(roman.compareTo(6L), equalTo(1));
        assertThat(roman.compareTo(7L), equalTo(0));
        assertThat(roman.compareTo(8L), equalTo(-1));

        //Tests compareTo sur FLOAT
        assertThat(roman.compareTo(6.0f), equalTo(1));
        assertThat(roman.compareTo(7.0f), equalTo(0));
        assertThat(roman.compareTo(8.0f), equalTo(-1));

        //Tests compareTo sur DOUBLE
        assertThat(roman.compareTo(6.0), equalTo(1));
        assertThat(roman.compareTo(7.0), equalTo(0));
        assertThat(roman.compareTo(8.0), equalTo(-1));

        //Tests compareTo sur INTEGER
        assertThat(roman.compareTo(6), equalTo(1));
        assertThat(roman.compareTo(7), equalTo(0));
        assertThat(roman.compareTo(8), equalTo(-1));
        
    }
	


    //Help you to handle exception. :-)
    public static Throwable exceptionOf(Callable<?> callable) {
        try {
            callable.call();
            return null;
        } catch (Throwable t) {
            return t;
        }
    }
}
